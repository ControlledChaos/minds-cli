=========
minds-cli
=========


.. image:: https://img.shields.io/pypi/v/minds-cli.svg
        :target: https://pypi.python.org/pypi/minds-cli

.. image:: https://img.shields.io/travis/granitosaurus/minds-cli.svg
        :target: https://travis-ci.org/granitosaurus/minds-cli

.. image:: https://readthedocs.org/projects/minds-cli/badge/?version=latest
        :target: https://minds-cli.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/granitosaurus/minds-cli/shield.svg
     :target: https://pyup.io/repos/github/granitosaurus/minds-cli/
     :alt: Updates


cli interface for minds-api.


* Free software: GNU General Public License v3
* Documentation: https://minds-cli.readthedocs.io.


Features
--------

* Support these commands::

    Commands:
      downvote       downvote content
      upvote         upvote content
      delete         delete content
      newsfeed       display newsfeed in the terminal
          boost       boost section
          channel     specific channel
          subscribed  subscribed section
          top         top section
      notifications  display notifications in the terminal
          all            show all notification categories
          comments       show only comment notifications
          groups         show only group notifications
          subscriptions  show only subscription notifications
          tags           show only tag notifications
          votes          show only vote notifications
      post           Create post/comment or display one
          blog      post a blog under current user's blogfeed
          comment   post a comment under a piece of content
          newsfeed  post a post under current user's newsfeed
      profile        Manage local user profiles
          list  list local profiles
          save  save profile locally from provided...
      show           Create post/comment or display one
          comments  show comments
          post      show single post

* Supports user profiles for easy sharing through multiple profiles.
* Prints formatted posts, newsfeeds and comments in terminal::

    $ minds newsfeed channel tinarg
    Working from Chiang Mai, Thailand this month!
    Got a condo with this view, pool and a gym for 400€ per month.
    Pretty lovely place so far!

    #chiangmai #thailand #digitalnomad #workspaceview
    https://www.minds.com/fs/v1/thumbnail/825548664578514944
    ---tinarg---4/0----------------------https://minds.com/newsfeed/825549491984670720-----------------------C:0---R:0---
* Posting supports input via editor, file or stdin with markdown conversion for blogs (yes you can write blogs in markdown!)

See docs for more: https://minds-cli.readthedocs.io.

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

If you wish to support this project you can open up an issue or send me a tip:

- Bitcoin: ``18oJWwdy1XExN69kggj2JdYtqohMPx9QF8``
- Bitcoin Cash: ``18oJWwdy1XExN69kggj2JdYtqohMPx9QF8``
- Doge: ``DEmJSgawKvtS9CGQUC7QdNETa2UuYBrenS``
- Ethereum: ``0xe8b452F9E3FDa8CEB32b2C231f73cC5cFa67735B``
- Monero: ``43PAxkfekEYQki3HTQkgt9eH7KK5ZRBd447HkumTGmo8cacA9vP25MwWbwaVe4vUMveKAzAiA4j8xgUi29TpKXpm3yjjWof``

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
